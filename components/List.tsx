import React from "react";

import { SimpleBook } from "../lib/books";

export default function List(props: Props) {
  return (
    <ul>
      {props.books.map((book, idx) => {
        return (
          <li key={`${idx}_${book.id}`}>
            {book.id}; {book.title}; {book.authors}; {book.isbn}
          </li>
        );
      })}
    </ul>
  );
}

interface Props {
  books: SimpleBook[];
}
