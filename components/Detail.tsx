import React from "react";

import { DetailedBook } from "../lib/books";

export default function Detail(props: Props) {
  return <p>{props.book.title}</p>;
}

interface Props {
  book: DetailedBook;
}
