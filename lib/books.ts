import sqlite3 from "sqlite3";

const DATABASE_PATH = "catalogue.db";

export async function getBook(id: number): Promise<DetailedBook> {
  const database = new sqlite3.Database(DATABASE_PATH, sqlite3.OPEN_READONLY);

  const sql =
    "SELECT isbn, lccn, title, subtitle, authors, edition, publisher, publish_year, publish_month, publish_location, pages, pagination, weight, last_updated FROM books WHERE id = ? AND deleted = 0";

  const book: Promise<DetailedBook> = new Promise((resolve, reject) => {
    database.get(sql, [id], (err: Error | null, book: DetailedBook) => {
      if (err) {
        reject(err);
      }
      if (book) {
        resolve(book);
      } else {
        reject(new Error("Not a valid id"));
      }
    });
  });

  database.close();
  return book;
}

export async function getAllBooks() {
  const database = new sqlite3.Database(DATABASE_PATH, sqlite3.OPEN_READONLY);

  const books = new Promise<SimpleBook[]>((resolve, reject) => {
    database.all(
      "SELECT id, title, authors, isbn FROM books WHERE deleted = 0",
      (error: Error | null, rows: SimpleBook[]) => {
        if (error) {
          reject(error);
        }
        resolve(rows);
      }
    );
  });

  database.close();

  return books;
}

export interface DetailedBook {
  isbn: string | null;
  lccn: string | null;
  title: string | null;
  authors: string | null;
  edition: string | null;
  publisher: string | null;
  publish_year: string | null;
  publish_month: string | null;
  publish_location: string | null;
  pages: string | null;
  pagination: string | null;
  weight: string | null;
  last_updated: Date | null;
  deleted: boolean | null;
}

export interface SimpleBook {
  id: number;
  title: string | null;
  authors: string | null;
  isbn: string | null;
}
