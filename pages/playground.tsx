import React from "react";

import Detail from "../components/Detail";
import List from "../components/List";
import { DetailedBook, SimpleBook, getBook, getAllBooks } from "../lib/books";

export default function Playground(props: Props) {
  return (
    <div>
      <h1>Detail</h1>
      <hr />
      <Detail book={props.book} />
      <h1>List</h1>
      <hr />
      <List books={props.books} />
    </div>
  );
}

export async function getServerSideProps() {
  return {
    props: {
      book: await getBook(44),
      books: await getAllBooks(),
    },
  };
}

interface Props {
  book: DetailedBook;
  books: SimpleBook[];
}
